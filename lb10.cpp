#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    const int n = 5; // Вказуйте рядок або стовбець матриці(вона квадратна)
    const int n1 = ((n * n) - n) / 2 + n, n2 = (n * n - n) / 2;
    int m[n][n], number=0, vihod1[n1], vihod2[n2], i, j;
    srand(time(NULL));

    for (i = 0; i < n; i++) // Введення даних в матрицю n * n
    {
        for(j=0; j<n; j++) 
            m[i][j]= rand() % 41 - 20;
    }
    
    cout << setw(5);
    for (i = 0; i < n; i++) // Виведення матриці
    {
        for (j = 0; j < n; j++)
            cout << m[i][j] << setw(5);
        cout << endl;
    }

    // Виведення першої матриці (верхній трикутник + діагональ)
    cout << "Matrix 1:" << endl ; 
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n;j++)
        {
            while (j >= i && j < n)
            {
                vihod1[number] = m[i][j];
                cout << vihod1[number] << setw(5);
                j++;
                number++;
            }
        }
    }   

    // Виведення другої матриці (нижній трикутник)
    cout << endl << "Matrix 2:" << endl;
    number = 0;
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            while (j < i)
            {
                vihod2[number] = m[i][j];
                cout << vihod2[number] << setw(5);
                j++;
                number++;
            }
        }
    }
}
